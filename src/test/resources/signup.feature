Feature: SignUp User

  Scenario: Verificar el registro de usuario
    Given la pagina "http://todo.ly" esta abierta
    When registro los datos
      | full_name | pepito dias         |
      | email     | pepe_007@gmail.com |
      | password  | 12345               |
    And acepto los terminos de uso
    And deberia iniciar sesion
      | email     | pepe_007@gmail.com |
      | password  | 12345 |
    Then ingresar a la web app
