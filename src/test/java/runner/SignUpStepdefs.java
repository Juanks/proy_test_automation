package runner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import page.LoginModal;
import page.MainPage;
import page.MenuSection;
import page.SignUpModal;
import session.Session;

import java.util.Map;

public class SignUpStepdefs {

    MainPage mainPage = new MainPage();

    LoginModal loginModal = new LoginModal();
    SignUpModal signUpModal = new SignUpModal();

    MenuSection menuSection = new MenuSection();
    @Given("la pagina {string} esta abierta")
    public void laPaginaEstaAbierta(String pUrl) {
        Session.getInstance().getBrowser().get(pUrl);
    }

    @When("registro los datos")
    public void registroLosDatos(Map<String, String> pDatosRegistro) {
        mainPage.signUpButton.click();
        signUpModal.fullNameTxtBox.writeText(pDatosRegistro.get("full_name"));
        signUpModal.emailTxtBox.writeText(pDatosRegistro.get("email"));
        signUpModal.passwordTxtBox.writeText(pDatosRegistro.get("password"));

    }

    @And("acepto los terminos de uso")
    public void aceptoLosTerminosDeUso() {
        signUpModal.aceptTermsCheckBox.click();
        signUpModal.signUpButton.click();
    }

    @And("deberia iniciar sesion")
    public void deberiaIniciarSesion(Map<String,String> nuevaCredencial) {
        menuSection.logoutButton.click();

        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText(nuevaCredencial.get("email"));
        loginModal.pwdTxtBox.writeText(nuevaCredencial.get("password"));
        loginModal.loginButton.click();

    }

    @Then("ingresar a la web app")
    public void ingresarALaWebApp() {
        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(),
                "Error en SignUp de usuario");
    }
}
