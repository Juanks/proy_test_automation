package runner;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;
import session.Session;

@RunWith(Cucumber.class)
public class HookLogin {
    @Before
    public void setup(){
        System.out.println("este es el before");
    }

    @After
    public void cleanup(){
        System.out.println("este es el after cerrar sesion");
        Session.getInstance().closeSession();
    }


}
