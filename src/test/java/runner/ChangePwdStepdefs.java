package runner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import page.*;
import session.Session;

import java.util.Map;

public class ChangePwdStepdefs {
    MainPage mainPage = new MainPage();

    LoginModal loginModal = new LoginModal();

    SignUpModal signUpModal = new SignUpModal();

    MenuSection menuSection = new MenuSection();

    SettingModal settingModal = new SettingModal();
    @Given("si la pagina {string} esta abierta")
    public void siLaPaginaEstaAbierta(String pUrl) {
        Session.getInstance().getBrowser().get(pUrl);
    }

    @And("ingreso las credenciales")
    public void ingresoLasCredenciales(Map<String,String> credenciales) {
        loginChange(credenciales.get("email"),credenciales.get("password"));
    }

    public void loginChange(String pEmail, String pPass){
        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText(pEmail);
        loginModal.pwdTxtBox.writeText(pPass);
        loginModal.loginButton.click();
    }

    @When("el usuario ingresa a la opcion setting cambiando su password por uno nuevo")
    public void elUsuarioIngresaALaOpcionSettingCambiandoSuPasswordPorUnoNuevo(Map<String,String> pPasswords) {
        menuSection.settingButton.click();

        settingModal.passOldTextBox.writeText(pPasswords.get("passwordOld"));
        settingModal.passNewTextBox.writeText(pPasswords.get("passwordNew"));
        settingModal.okButton.click();

    }

    @And("cierra sesion con logout")
    public void cierraSesionConLogout() {
        menuSection.logoutButton.click();
    }

    @Then("verifica el cambio al cambio ingresando a la webApp")
    public void verficaElCambioAlCambioIngresandoALaWebApp(Map<String,String> credenciales) {
        loginChange(credenciales.get("email"),credenciales.get("password"));
        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(),
                "ERROR, fallo change Password ");
    }
}
