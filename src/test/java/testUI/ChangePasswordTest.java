package testUI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import page.*;
import session.Session;

public class ChangePasswordTest {

    MainPage mainPage = new MainPage();
    LoginModal loginModal = new LoginModal();
    MenuSection menuSection= new MenuSection();
    SettingModal settingModal = new SettingModal();

    @BeforeEach
    public void goWebApp(){
        Session.getInstance().getBrowser().get("http://todo.ly/");
    }
    @AfterEach
    public void closeWebApp() throws InterruptedException {
        Thread.sleep(5000);
        Session.getInstance().closeSession();
    }

    @Test
    public void verficarChangePass(){
        String vEmail = "alavjc@gmail.com";
        String vPassOld = "12345";
        String vPassNew = "123456";

        loginChange(vEmail, vPassOld);

        menuSection.settingButton.click();

        settingModal.passOldTextBox.writeText(vPassOld);
        settingModal.passNewTextBox.writeText(vPassNew);
        settingModal.okButton.click();

        menuSection.logoutButton.click();

        loginChange(vEmail, vPassNew);

        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(),
                "ERROR, fallo change Password ");
    }

    public void loginChange(String pEmail, String pPass){
        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText(pEmail);
        loginModal.pwdTxtBox.writeText(pPass);
        loginModal.loginButton.click();
    }

}
