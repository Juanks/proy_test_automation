package testUI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import page.*;
import session.Session;

public class SignUpUserTest {

    MainPage mainPage = new MainPage();
    LoginModal loginModal = new LoginModal();
    MenuSection menuSection= new MenuSection();

    SignUpModal signUpModal = new SignUpModal();

    @BeforeEach
    public void goWebApp(){
        Session.getInstance().getBrowser().get("http://todo.ly/");
    }
    @AfterEach
    public void closeWebApp() throws InterruptedException {
        Thread.sleep(5000);
        Session.getInstance().closeSession();
    }

    @Test
    public void verificarSignUpUser(){
        int numero1 = (int)(Math.random()*10+1);
        int numero2 = (int)(Math.random()*10+1);
        String emailRamdom = "luck1"+numero1+"_"+numero2+"@gmail.com";
        String pwd = "12345";

        mainPage.signUpButton.click();
        signUpModal.fullNameTxtBox.writeText("Lucas Film");
        signUpModal.emailTxtBox.writeText(emailRamdom);
        signUpModal.passwordTxtBox.writeText(pwd);
        signUpModal.aceptTermsCheckBox.click();
        signUpModal.signUpButton.click();

        menuSection.logoutButton.click();

        login(emailRamdom, pwd);

        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(),
                "Error en SignUp de usuario");
    }

    public void login(String pEmail, String pPass){
        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText(pEmail);
        loginModal.pwdTxtBox.writeText(pPass);
        loginModal.loginButton.click();
    }

}
