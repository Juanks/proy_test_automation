package page;

import control.Button;
import control.TextBox;
import org.openqa.selenium.By;

public class SettingModal {
    public TextBox passOldTextBox = new TextBox(By.id("TextPwOld"));
    public TextBox passNewTextBox = new TextBox(By.id("TextPwNew"));
    public Button okButton = new Button(By.xpath("//span[text()='Ok']"));

}
