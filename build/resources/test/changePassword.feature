Feature: Cambio de password

  Scenario: Verificar cambio de password
    Given si la pagina "http://todo.ly" esta abierta
    And ingreso las credenciales
      | email    | alavjc1@gmail.com |
      | password | 12345          |
    When el usuario ingresa a la opcion setting cambiando su password por uno nuevo
      | passwordOld | 12345 |
      | passwordNew | 123456  |
    And cierra sesion con logout
    Then verifica el cambio al cambio ingresando a la webApp
      | email    | alavjc1@gmail.com |
      | password | 123456           |